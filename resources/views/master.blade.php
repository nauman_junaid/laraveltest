<!DOCTYPE html>
<!--[if lt IE 7]>  <html lang="en" class="ie ie6 lte9 lte8 lte7"> <![endif]-->
<!--[if IE 7]>     <html lang="en" class="ie ie7 lte9 lte8 lte7"> <![endif]-->
<!--[if IE 8]>     <html lang="en" class="ie ie8 lte9 lte8"> <![endif]-->
<!--[if IE 9]>     <html lang="en" class="ie ie9 lte9"> <![endif]-->
<!--[if gt IE 9]>  <html lang="en"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <!--[if IE]> <meta http-equiv="X-UA-Compatible" content="IE=edge" /> <![endif]-->

  <title>@yield('title')</title>

  <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css" media="all" />
  <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-theme.min.css" media="all" />
  <meta name="csrf-token" content="<?= csrf_token() ?>"/>

  <!--[if lt IE 9]>
  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
</head>

<body>
<div class="page-container">
  <div class="container-fluid">
    @yield('content')
  </div>
</div>
  <script type="text/javascript" src="/assets/js/jquery.min.js"></script>
  <script type="text/javascript" src="/assets/js/bootstrap.min.js"></script>
  <script type="text/javascript">
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });

  $('.listings').on('click', '.cancel-update', function() {
    $('.edit-form ').remove();
  });

  $('.listings').on('click', '.product-update', function() {
    $('#loader').show();
    var id= $('input[name=edit_product_id]').val();
    var productName = $('input[name=edit_product_name]').val();
    var productQuantity = $('input[name=edit_quantity_in_stock]').val();
    var productPrice = $('input[name=edit_price_per_item]').val();

    var url = '/products/update';
    var type = 'PUT';
    $.ajax({
      url: url,
      type: type,
      dataType: 'json',
      data: {
        'id': id,
        'product_name': productName,
        'quantity_in_stock': productQuantity,
        'price_per_item': productPrice,
      },
      success: function (data, status, jqXHR) {
        $('.listings').html(data.view);
        $('#loader').hide();
      },
      error: function(jqXHR, Status, error) {
        $('#loader').hide();
      }
    });
  });

  function createForm(el, productId, productName, productQuantity, productPrice) {
    var form = '<form class="form-horizontal  edit-form">';
      form += '<input value="'+ productName +'" name="edit_product_name" type="text" placeholder="Product Name" class="form-control input-sm-3" required="">';
      form += '<input value="'+ productQuantity +'" name="edit_quantity_in_stock" type="text" placeholder="Product Name" class="form-control input-sm-3" required="">';
      form += '<input value="'+ productPrice +'" name="edit_price_per_item" type="text" placeholder="Product Name" class="form-control input-sm-3" required="">';
      form += '<input type="hidden" value="'+ productId +'" name="edit_product_id" />';
      form += '<input type="button" value="Edit Product" class="btn btn-primary product-update" />';
      form += '<input type="button" value="Cancel" class="btn btn-primary cancel-update" />';
      form += '</form>';
    $(el).parent().append(form);
  }
  </script>

  @yield('script')
</body>
</html>
