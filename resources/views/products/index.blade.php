@extends('master')
@section('title', 'Add Products')

@section('content')
<form class="form-horizontal" action='/products/store' method="POST">
<fieldset>

<!-- Form Name -->
<legend>Products</legend>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="Product Name">Product Name</label>  
  <div class="col-md-4">
  <input id="Product Name" name="product_name" type="text" placeholder="Product Name" class="form-control input-md" required="">
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="Quantity In Stock">Quantity In Stock</label>  
  <div class="col-md-4">
  <input id="Quantity In Stock" name="quantity_in_stock" type="text" placeholder="Quantity In Stock" class="form-control input-md" required="">
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="Price Per Item">Price Per Item</label>
  <div class="col-md-4">
  <input id="Price Per Item" name="price_per_item" type="text" placeholder="Price Per Item" class="form-control input-md" required="">
  </div>
</div>

<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for=""></label>
  <div class="col-md-4">
  	<input type="hidden" name="_token" value="{{ csrf_token() }}">
    <button id="" name="" class="btn btn-primary">Add Product</button>
  </div>
</div>

</fieldset>
</form>

<div class='listings'>
	@include('products.listing')
</div>

@stop