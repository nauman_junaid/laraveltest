<?xml version="1.0" encoding="UTF-8"?>
<Products>
  <Product>
    <id><?= $product->id ?></id>
    <productName><?= $product->product_name ?></productName>
    <productQuantity><?= $product->quantity_in_stock ?></productQuantity>
    <productItemPrice><?= $product->price_per_item ?></productItemPrice>
    <updateAt><?= $product->updated_at ?></updateAt>
    <createdAt><?= $product->created_at ?></createdAt>
  </Product>
</Products>