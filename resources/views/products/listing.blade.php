<?php if ($products) { ?>
<div class='col-lg-12'>
	<img id="loader" src="/assets/img/loader.gif" style="display: none" />
	<div class="row">
		<span style='display: inline-block; width: 10%;'><strong>#</strong></span>
		<span style='display: inline-block; width: 10%;'><strong>Product</strong></span>
		<span style='display: inline-block; width: 10%;'><strong>Stock Quantity</strong></span>
		<span style='display: inline-block; width: 10%;'><strong>Item Price</strong></span>
		<span style='display: inline-block; width: 10%;'><strong>Total Value Number</strong></span>
		<span style='display: inline-block; width: 12%;'><strong>Updated At</strong></span>
		<span style='display: inline-block; width: 12%;'><strong>Created At</strong></span>
	</div>
	<?php $totalValue = 0; $i = 1;
	foreach ($products as $product) { 
		$productName = $product->product_name;
		$stockQuantity = $product->quantity_in_stock;
		$itemPrice = $product->price_per_item;
		$totalValueNumber = $stockQuantity * $itemPrice;
		$updatedAt = $product->updated_at;
		$createdAt = $product->created_at;
		$totalValue += $totalValueNumber;
		?>
	<div class="row" id='<?= $product->id ?>'>
		<span class='product-edit' title='edit' style='display: inline-block; width: 10%; cursor:pointer; color: blue'><?= $i ?></span>
		<span class='product-name' style='display: inline-block; width: 10%;'><?= $productName ?></span>
		<span class='product-quantity' style='display: inline-block; width: 10%;'><?= $stockQuantity ?></span>
		<span class='product-item-price' style='display: inline-block; width: 10%;'><?= $itemPrice ?></span>
		<span class='product-value-number' style='display: inline-block; width: 10%;'><?= $totalValueNumber ?></span>
		<span class='' style='display: inline-block; width: 12%;'><?= $updatedAt ?></span>
		<span class='' style='display: inline-block; width: 12%;'><?= $createdAt ?></span>
	</div>
	<?php $i++;
} ?>
	<div class="row">
		<span style='display: inline-block; width: 10%;'></span>
		<span style='display: inline-block; width: 10%;'></span>
		<span style='display: inline-block; width: 10%;'></span>
		<span style='display: inline-block; width: 10%;'><strong>Total Sum</strong></span>
		<span style='display: inline-block; width: 10%;'><?= $totalValue ?></span>
	</div>
</div>
<?php
} ?>

@section('script')
<script type="text/javascript">
  $('.listings').on('click', '.product-edit', function() {
    $('.edit-form ').remove();
    var productId = $(this).parent().attr('id');
    var productName = $(this).parent().find('.product-name').html();
    var productQuantity = $(this).parent().find('.product-quantity').html();
    var productPrice = $(this).parent().find('.product-item-price').html();
    createForm(this, productId, productName, productQuantity, productPrice);
});
</script>
@stop
