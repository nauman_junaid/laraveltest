<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use View;
use File;
use Response;
use App\Models\Product;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $products = Product::all()->sortByDesc('updated_at');
        return View::make('products.index', ['products' => $products]);
    }

    /**
     * Store a newly updated resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $product = new Product();

        $input = $request->all();

        $product->fill($input);
        $product->save();

        if ($product->id) {
            if (!file_exists(public_path('/assets/files/json/'))) {
                mkdir(public_path('/assets/files/json/'), 0777, true);
            }
            if (!file_exists(public_path('/assets/files/xml/'))) {
                mkdir(public_path('/assets/files/xml/'), 0777, true);
            }

            $fileName = $product->id . '-products.json';
            File::put(public_path('/assets/files/json/'.$fileName), json_encode($product));
            $fileName = $product->id . '-products.xml';
            $xml = View::make('products.xml.index', ['product' => $product])->render();
            File::put(public_path('/assets/files/xml/'.$fileName), $xml);
        }

        return redirect('/products');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $input = $request->all();
        $product = Product::find($input['id']);
        unset($input['id']);
        $product->fill($input);
        $product->save();

        $products = Product::all()->sortByDesc('updated_at');
        $view = View::make('products.listing', ['products' => $products])->render();
        return Response::json(['view' => $view]);
    }
}
