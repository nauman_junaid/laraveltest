<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::resource('products', 'ProductsController');
Route::get('/products', ['as' => 'products.index', 'uses' => 'ProductsController@index']);
Route::post('/products/store', ['as' => 'products.store', 'uses' => 'ProductsController@store']);
Route::put('/products/update', ['as' => 'products.store', 'uses' => 'ProductsCoutoller@store']);


